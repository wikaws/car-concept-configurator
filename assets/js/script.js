// const elToggle  = document.querySelector(".btnInfo");
const elBtnColor  = document.querySelectorAll(".btnColor");
const elToggle  = document.querySelector(".arButtonUI");
const elContent = document.querySelector("p.text");
const elCustomizer = document.querySelector(".customizerUI");
const popUpDesc = document.getElementById("popUpDesc");
const popUpCloseBtn = document.querySelector(".popUpCloseBtn");
const swipeBtn = document.querySelector(".swipeButton");
const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
var colorHex = 'ffffff';

elToggle.addEventListener("click", () => {
    if (!isMobile) {
        // elContent.classList.toggle("hide");
        popUpDesc.style.display = 'flex';
        document.getElementById("qrcode-image").src = 'assets/images/qrcode-'+colorHex+'.png';
    }

    if(isMobile) {
        window.location.href = 'https://vertex-car-configuration.vercel.app/ARConfiguratorMarkerless.html?colorHex='+colorHex;
    }
});

popUpCloseBtn.addEventListener("click", () => {
    popUpDesc.style.display = 'none';
});

swipeBtn.addEventListener("click", () => {
    elCustomizer.classList.toggle("show");
});

elBtnColor.forEach((clkColor, idx) => {
    clkColor.addEventListener("click", () => {
        switch (idx) {
            case 0:
                colorHex = 'ffffff'
                break;
            case 1:
                colorHex = '000000'
                break;
            case 2:
                colorHex = '31C4C4'
                break;
            case 3:
                colorHex = 'C32A2A'
                break;
        
            default:
                break;
        }
    })
});